﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace iChatRepository.Interfaces
{
   public interface IBase<T> where T : class
    {
        IEnumerable<T> GetAll();

        T FindSingle(int id);
        
        IEnumerable<T> FindBy(Expression<Func<T,bool>> predicate);

        void Add(T toAdd);

        void  Update(T toUpdate);

        void Delete(int id);

        void Delete(T toDelete);     

    }
}
