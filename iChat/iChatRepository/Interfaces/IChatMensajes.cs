﻿using iChatEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatRepository.Interfaces
{
    public interface IChatMensajes : IBase<ChatMensaje>
    {
        IEnumerable<ChatMensaje> GetChatMessajeById(int id);
    }
}
