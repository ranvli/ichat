﻿using iChatEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatRepository.Interfaces
{
    public interface IUsuario : IBase<Usuario>
    {

        IEnumerable<Usuario> GetUsersByChatRoomID(int id);
        IEnumerable<Usuario> GetUsuariosEnLinea(int ChatId);

    }
}
