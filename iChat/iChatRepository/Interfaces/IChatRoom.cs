﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iChatEntities;

namespace iChatRepository.Interfaces
{
    public interface IChatRoom : IBase<ChatRoom>
    {
        IEnumerable<ChatRoomInfo> GetChatsInfo();

      
    }
}
