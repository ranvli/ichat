﻿using iChatEntities;
using iChatRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace iChatRepository.Repositories
{
    class UsuarioRepository : IUsuario
    {
        
        protected DbContext _entities;
        protected readonly DbSet<Usuario> _dbset;
        public UsuarioRepository(DbContext context) //: base(context)
        {
            this._entities = context;
           
            _dbset = context.Set<Usuario>();
        }

        public void Add(Usuario toAdd)
        {
            _dbset.Add(toAdd);
        }

        public void Delete(int id)
        {
            var usuario = FindSingle(id);

            _dbset.Remove(usuario);
        }

        public void Delete(Usuario toDelete)
        {
            _dbset.Remove(toDelete);
        }

        public IEnumerable<Usuario> FindBy(Expression<Func<Usuario, bool>> predicate)
        {
            return _dbset.Where(predicate).ToList();
        }

        public Usuario FindSingle(int id)
        {
            return _dbset.Where(r => r.UsuarioID == id).FirstOrDefault();
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _dbset.AsEnumerable();
        }

        public IEnumerable<Usuario> GetUsersByChatRoomID(int id)
        {
            var user = (from t in _entities.Set<ChatRoomUsuario>()
                       from s in _entities.Set<Usuario>()
                       where  t.ChatRoomID == id && s.UsuarioID == t.UsuarioID
                       select s).ToList();                     
            return user;
        }
        public IEnumerable<Usuario> GetUsuariosEnLinea(int ChatId)
        {
            var user = (from t in _entities.Set<ChatRoomUsuario>()
                        from s in _entities.Set<Usuario>()
                        where t.EnLinea == true && t.ChatRoomID == ChatId && s.UsuarioID == t.UsuarioID
                        select s).ToList();
            return user;
        }
        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Usuario toUpdate)
        {
            _entities.Entry(toUpdate).State = EntityState.Modified;
        }
    }
}
