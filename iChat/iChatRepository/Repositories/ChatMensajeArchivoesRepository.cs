﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using iChatEntities;
using iChatRepository.Interfaces;


namespace iChatRepository.Repositories
{
    class ChatMensajeArchivoesRepository : IChatMensajeArchivoes
    {
        protected DbContext _entities;
        protected readonly DbSet<ChatMensajeArchivo> _dbset;
        public ChatMensajeArchivoesRepository(DbContext context) //: base(context)
        {
            this._entities = context;

            _dbset = context.Set<ChatMensajeArchivo>();
        }


        public void Add(ChatMensajeArchivo toAdd)
        {
            _dbset.Add(toAdd);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(ChatMensajeArchivo toDelete)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChatMensajeArchivo> FindBy(Expression<Func<ChatMensajeArchivo, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public ChatMensajeArchivo FindSingle(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChatMensajeArchivo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(ChatMensajeArchivo toUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
