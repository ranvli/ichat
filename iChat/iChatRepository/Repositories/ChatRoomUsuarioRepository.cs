﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using iChatEntities;
using iChatRepository.Interfaces;

namespace iChatRepository.Repositories
{
    class ChatRoomUsuarioRepository : IChatRoomUsuarios
    {

        protected DbContext _entities;
        protected readonly DbSet<ChatRoomUsuario> _dbset;
        public ChatRoomUsuarioRepository(DbContext context) //: base(context)
        {
            this._entities = context;
            _dbset = context.Set<ChatRoomUsuario>();
        }


        public void Add(ChatRoomUsuario toAdd) 
        {

            _dbset.Add(toAdd);
        }

        public void Delete(int id)
        {
            var aux = FindSingle(id);

            _dbset.Remove(aux);
        }

        public void Delete(ChatRoomUsuario toDelete)
        {
            _dbset.Remove(toDelete);
        }

        public IEnumerable<ChatRoomUsuario> FindBy(Expression<Func<ChatRoomUsuario, bool>> predicate)
        {
            return _dbset.Where(predicate).ToList();
        }

        public ChatRoomUsuario FindSingle(int id)
        {
            return _dbset.Where(r => r.ChatRoomUsuarioID == id).FirstOrDefault();
        }

        public IEnumerable<ChatRoomUsuario> GetAll()
        {
            return _dbset.AsEnumerable();
        }

        public void Update(ChatRoomUsuario toUpdate)
        {
            _entities.Entry(toUpdate).State = EntityState.Modified;
        }
        public void FueraDeLinea(int UsuarioId,int ChatId)
        {
            try
            {
             
                var usuarioChat = _dbset.Where(u => u.UsuarioID == UsuarioId && u.ChatRoomID==ChatId).FirstOrDefault();
                usuarioChat.EnLinea = false;
                _entities.Entry(usuarioChat).State = EntityState.Modified;

            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
