using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using iChatRepository.Interfaces;


namespace iChatRepository.Repositories
{
    public  class BaseRepository<T> : IBase<T> where T : class
    {

          protected DbContext _entities;
          protected readonly DbSet<T> _dbset;


   
        public BaseRepository(DbContext context)
        {
            this._entities = context;
            _dbset = context.Set<T>();

        }

        public BaseRepository()
        {

        }


        public virtual void Add(T toAdd)
        {
            _dbset.Add(toAdd);
        }

        public virtual void Delete(int id)
        {
            T entity = FindSingle(id);
            _dbset.Remove(entity);
        }

        public virtual void Delete(T toDelete)
        {
            _dbset.Remove(toDelete);
             
        }

        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {

           IEnumerable<T> query = _dbset.Where(predicate).AsEnumerable();

            return query;
        }

        public virtual T FindSingle(int id)
        {
            return _dbset.Find(id);

         }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbset.AsEnumerable<T>();
        }

        public virtual void Update(T toUpdate)
        {
            _entities.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
        }

    }
}
