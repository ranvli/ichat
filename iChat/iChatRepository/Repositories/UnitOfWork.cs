using iChatEntities;
using System;
using iChatRepository.Interfaces;
using iChatShare;

namespace iChatRepository.Repositories
{

    public interface IUnitOfWork : IDisposable
    {
        IChatRoom ChatRoomRepository { get; }
        IUsuario UsuarioRepository { get; }
        IChatMensajes  ChatMensajesRepository { get; }
        IChatRoomUsuarios ChatRoomUsuarios { get; }


        void Save();
    }



    public partial class UnitOfWork : IUnitOfWork
    {
        private ChatRoomRepository   _ChatRoomRepository;
        private UsuarioRepository    _UsuarioRepositorio;
        private ChatMensajesRepository _ChatMensajesRespository;
        private ChatMensajeArchivoesRepository _ChatMensajeArchivoesRespository;
        private ChatRoomUsuarioRepository _ChatRoomUsuarios;
        private iChatModelContainer  _context;

        //Add any new repository here 

        public IChatRoom ChatRoomRepository
        {
            get
            {

                if (_ChatRoomRepository == null)

                    _ChatRoomRepository = new ChatRoomRepository(_context);

                return _ChatRoomRepository;
            }
        }

        public IUsuario UsuarioRepository
        {
            get
            {

                if (_UsuarioRepositorio == null)

                    _UsuarioRepositorio= new UsuarioRepository(_context);

                return _UsuarioRepositorio;
            }
        }

        public IChatMensajes ChatMensajesRepository
        {
            get
            {
                 if (_ChatMensajesRespository == null)

                   _ChatMensajesRespository= new ChatMensajesRepository(_context);

                return _ChatMensajesRespository;
            }
        }

        public IChatMensajeArchivoes  ChatMensajesArchivoesRepository
        {
            get
            {
                if (_ChatMensajeArchivoesRespository  == null)

                    _ChatMensajeArchivoesRespository = new ChatMensajeArchivoesRepository(_context);

                return _ChatMensajeArchivoesRespository;
            }
        }

        public IChatRoomUsuarios ChatRoomUsuarios
        {
            get
            {
                if (_ChatRoomUsuarios == null)

                    _ChatRoomUsuarios = new ChatRoomUsuarioRepository(_context);

                return _ChatRoomUsuarios;
            }
        }



        public UnitOfWork()
        {
          _context = new iChatModelContainer(DataBaseHelpers.GetEntityConnection());
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
