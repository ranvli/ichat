﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using iChatEntities;
using iChatRepository.Interfaces;

namespace iChatRepository.Repositories
{
    class ChatMensajesRepository : IChatMensajes
    {
        protected DbContext _entities;
        protected readonly DbSet<ChatMensaje> _dbset;
        public ChatMensajesRepository(DbContext context) //: base(context)
        {
            this._entities = context;
           _dbset = context.Set<ChatMensaje>();
        }

        public void Add(ChatMensaje toAdd)
        {
            _dbset.Add(toAdd);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(ChatMensaje toDelete)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChatMensaje> FindBy(Expression<Func<ChatMensaje, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public ChatMensaje FindSingle(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChatMensaje> GetAll()
        {
            return _entities.Set<ChatMensaje>().AsEnumerable();
        }

        public IEnumerable<ChatMensaje> GetChatMessajeById(int id)
        {
            return (from t in _entities.Set<ChatMensaje>() where t.ChatMensajeID > id select t).ToList();
        }

       
        public void Update(ChatMensaje toUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
