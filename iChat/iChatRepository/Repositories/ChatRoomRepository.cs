﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using iChatEntities;
using iChatRepository.Interfaces;

namespace iChatRepository.Repositories
{
    public class ChatRoomRepository : IChatRoom 
    {
        protected DbContext _entities;
        protected readonly DbSet<ChatRoom> _dbset;
        
        public ChatRoomRepository(DbContext context) //: base(context)
        {
            this._entities = context;
            _dbset = context.Set<ChatRoom>();
        }
       
        public void Add(ChatRoom toAdd)
        {
            _dbset.Add(toAdd);
        }

        public void Delete(int id)
        {
          
        }

        public void Delete(ChatRoom toDelete)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChatRoom> FindBy(Expression<Func<ChatRoom, bool>> predicate)
        {
            return _dbset.Where(predicate).ToList();
        }

        public ChatRoom FindSingle(int id)
        {
          return  _dbset.Where(r => r.ChatRoomID == id).FirstOrDefault();
        }

        public IEnumerable<ChatRoom> GetAll()
        {
            return _entities.Set<ChatRoom>().AsEnumerable();
        }
        
        public IEnumerable<ChatRoomInfo> GetChatsInfo()
        {
           var chats = from chat in GetAll().ToList()

                        from chatUsuarios in chat.ChatRoomUsuarios
                            where chat.ChatRoomID == chatUsuarios.ChatRoomID &&
                                    chatUsuarios.Creador == true
                            select new ChatRoomInfo
                            {
                                Nombre = chat.Nombre,
                                Descripcion = chat.Descripcion,
                                Activo = chat.Activo,
                                CorreoUsuarioCreacion = chatUsuarios.Usuario.Correo,
                                FechaCreacion = (chatUsuarios.FechaCreacion.HasValue) ? chatUsuarios.FechaCreacion.Value : DateTime.MinValue

                            };
            return chats;
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(ChatRoom toUpdate)
        {
               //_dbset. = EntityState.Modified;
        }
    }
}
