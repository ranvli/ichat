
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/21/2018 11:44:22
-- Generated from EDMX file: C:\git\iChat\ichat\iChat\iChatDAL\iChatModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Chat1org];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ChatMensajeChatMensajeArchivo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChatMensajeArchivo] DROP CONSTRAINT [FK_ChatMensajeChatMensajeArchivo];
GO
IF OBJECT_ID(N'[dbo].[FK_ChatRoomChatMensaje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChatMensaje] DROP CONSTRAINT [FK_ChatRoomChatMensaje];
GO
IF OBJECT_ID(N'[dbo].[FK_ChatRoomUsuario_ChatRoom]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChatRoomUsuario] DROP CONSTRAINT [FK_ChatRoomUsuario_ChatRoom];
GO
IF OBJECT_ID(N'[dbo].[FK_ChatRoomUsuario_Usuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChatRoomUsuario] DROP CONSTRAINT [FK_ChatRoomUsuario_Usuario];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuarioChatMensaje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChatMensaje] DROP CONSTRAINT [FK_UsuarioChatMensaje];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ChatMensaje]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChatMensaje];
GO
IF OBJECT_ID(N'[dbo].[ChatMensajeArchivo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChatMensajeArchivo];
GO
IF OBJECT_ID(N'[dbo].[ChatRoom]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChatRoom];
GO
IF OBJECT_ID(N'[dbo].[ChatRoomUsuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChatRoomUsuario];
GO
IF OBJECT_ID(N'[dbo].[Usuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuario];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ChatMensajes'
CREATE TABLE [dbo].[ChatMensajes] (
    [ChatMensajeID] bigint IDENTITY(1,1) NOT NULL,
    [Texto] nvarchar(max)  NOT NULL,
    [ChatRoomID] int  NOT NULL,
    [UsuarioCreacionID] int  NOT NULL,
    [FechaCreacion] datetime  NULL,
    [Usuario_UsuarioID] int  NOT NULL
);
GO

-- Creating table 'ChatMensajeArchivoes'
CREATE TABLE [dbo].[ChatMensajeArchivoes] (
    [ChatMensajeArchivoID] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Peso] nvarchar(max)  NOT NULL,
    [Extension] nvarchar(max)  NOT NULL,
    [ChatMensajeID] bigint  NOT NULL
);
GO

-- Creating table 'ChatRooms'
CREATE TABLE [dbo].[ChatRooms] (
    [ChatRoomID] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Usuarios'
CREATE TABLE [dbo].[Usuarios] (
    [UsuarioID] int IDENTITY(1,1) NOT NULL,
    [NickName] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'ChatRoomUsuarios'
CREATE TABLE [dbo].[ChatRoomUsuarios] (
    [ChatRoomUsuarioID] int  NOT NULL,
    [UsuarioID] int  NOT NULL,
    [ChatRoomID] int  NOT NULL,
    [EnLinea] bit  NULL,
    [Creador] bit  NULL,
    [FechaCreacion] datetime  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ChatMensajeID] in table 'ChatMensajes'
ALTER TABLE [dbo].[ChatMensajes]
ADD CONSTRAINT [PK_ChatMensajes]
    PRIMARY KEY CLUSTERED ([ChatMensajeID] ASC);
GO

-- Creating primary key on [ChatMensajeArchivoID] in table 'ChatMensajeArchivoes'
ALTER TABLE [dbo].[ChatMensajeArchivoes]
ADD CONSTRAINT [PK_ChatMensajeArchivoes]
    PRIMARY KEY CLUSTERED ([ChatMensajeArchivoID] ASC);
GO

-- Creating primary key on [ChatRoomID] in table 'ChatRooms'
ALTER TABLE [dbo].[ChatRooms]
ADD CONSTRAINT [PK_ChatRooms]
    PRIMARY KEY CLUSTERED ([ChatRoomID] ASC);
GO

-- Creating primary key on [UsuarioID] in table 'Usuarios'
ALTER TABLE [dbo].[Usuarios]
ADD CONSTRAINT [PK_Usuarios]
    PRIMARY KEY CLUSTERED ([UsuarioID] ASC);
GO

-- Creating primary key on [ChatRoomUsuarioID] in table 'ChatRoomUsuarios'
ALTER TABLE [dbo].[ChatRoomUsuarios]
ADD CONSTRAINT [PK_ChatRoomUsuarios]
    PRIMARY KEY CLUSTERED ([ChatRoomUsuarioID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ChatMensajeID] in table 'ChatMensajeArchivoes'
ALTER TABLE [dbo].[ChatMensajeArchivoes]
ADD CONSTRAINT [FK_ChatMensajeChatMensajeArchivo]
    FOREIGN KEY ([ChatMensajeID])
    REFERENCES [dbo].[ChatMensajes]
        ([ChatMensajeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChatMensajeChatMensajeArchivo'
CREATE INDEX [IX_FK_ChatMensajeChatMensajeArchivo]
ON [dbo].[ChatMensajeArchivoes]
    ([ChatMensajeID]);
GO

-- Creating foreign key on [ChatRoomID] in table 'ChatMensajes'
ALTER TABLE [dbo].[ChatMensajes]
ADD CONSTRAINT [FK_ChatRoomChatMensaje]
    FOREIGN KEY ([ChatRoomID])
    REFERENCES [dbo].[ChatRooms]
        ([ChatRoomID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChatRoomChatMensaje'
CREATE INDEX [IX_FK_ChatRoomChatMensaje]
ON [dbo].[ChatMensajes]
    ([ChatRoomID]);
GO

-- Creating foreign key on [Usuario_UsuarioID] in table 'ChatMensajes'
ALTER TABLE [dbo].[ChatMensajes]
ADD CONSTRAINT [FK_UsuarioChatMensaje]
    FOREIGN KEY ([Usuario_UsuarioID])
    REFERENCES [dbo].[Usuarios]
        ([UsuarioID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioChatMensaje'
CREATE INDEX [IX_FK_UsuarioChatMensaje]
ON [dbo].[ChatMensajes]
    ([Usuario_UsuarioID]);
GO

-- Creating foreign key on [ChatRoomID] in table 'ChatRoomUsuarios'
ALTER TABLE [dbo].[ChatRoomUsuarios]
ADD CONSTRAINT [FK_ChatRoomUsuario_ChatRoom]
    FOREIGN KEY ([ChatRoomID])
    REFERENCES [dbo].[ChatRooms]
        ([ChatRoomID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChatRoomUsuario_ChatRoom'
CREATE INDEX [IX_FK_ChatRoomUsuario_ChatRoom]
ON [dbo].[ChatRoomUsuarios]
    ([ChatRoomID]);
GO

-- Creating foreign key on [UsuarioID] in table 'ChatRoomUsuarios'
ALTER TABLE [dbo].[ChatRoomUsuarios]
ADD CONSTRAINT [FK_ChatRoomUsuario_Usuario]
    FOREIGN KEY ([UsuarioID])
    REFERENCES [dbo].[Usuarios]
        ([UsuarioID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChatRoomUsuario_Usuario'
CREATE INDEX [IX_FK_ChatRoomUsuario_Usuario]
ON [dbo].[ChatRoomUsuarios]
    ([UsuarioID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------