﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace iChat.Controllers.ActionFilters
{
    public class ValidaSesionActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Este metodo se ejecuta siempre antes de cualquier acción
            // Si el usuario no esta logueado o la sesión expiro , lo devuelve a loguearse a la pantalla de login.

            if (filterContext.HttpContext.Session["Usuario"] == null)
            {
               
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Index");
                redirectTargetDictionary.Add("controller", "Home");
                redirectTargetDictionary.Add("area", "");

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }
            base.OnActionExecuting(filterContext);

        }
    }
}