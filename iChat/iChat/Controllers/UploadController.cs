﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;
using iChatRepository;
using iChatRepository.Repositories;
using iChatEntities;

namespace iChat.Controllers
{
    

    public class UploadController : Controller
    {
        UnitOfWork uOw = new UnitOfWork();

        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = System.Configuration.ConfigurationManager.AppSettings["ServerFiles"];
                    

                    if (!Directory.Exists(_path))
                        Directory.CreateDirectory(_path);
                    
                    _path = Path.Combine(_path, _FileName);
                    file.SaveAs(_path);


                    FileInfo fi = new FileInfo(_path);
                    string nombre, extension = "";
                    long tamano;
                    nombre = fi.Name;
                    extension = fi.Extension;
                    tamano = fi.Length /1024;

                    ChatMensajeArchivo cMa = new ChatMensajeArchivo();
                    cMa.ChatMensajeID = 4;
                    cMa.ChatMensajeArchivoID = 1;
                    cMa.Nombre = _path;
                    cMa.Extension = extension;
                    cMa.Peso = Convert.ToString(tamano);
                    

                    uOw.ChatMensajesArchivoesRepository.Add(cMa);
                    uOw.Save();
                   
                }
                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = "File upload failed!! " + ex.InnerException.InnerException.Message  ;
                return View();
            }
        }


    }
}