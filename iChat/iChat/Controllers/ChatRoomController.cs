﻿

using iChat.Controllers.ActionFilters;
using iChat.Models;
using iChatEntities;
using iChatRepository.Repositories;
using iChatShare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace iChat.Controllers
{
    public class ChatRoomController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();

       

        // GET: ChatRoom
        public ActionResult Index()
        {

            return PartialView();
        }

        // GET: ChatRoom/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [ValidaSesionActionFilter]
        public ActionResult Room(int id)
        {

            ChatRoom currentChat = unitOfWork.ChatRoomRepository.FindSingle(id);
            var usuariosActivos = currentChat.ChatRoomUsuarios.Where(u => u.EnLinea == true);
            currentChat.ChatRoomUsuarios = usuariosActivos.ToList();
            return View(currentChat);
        }



        // GET: ChatRoom/Create
        public ActionResult Create()
        {
            return View();

        }

        // POST: ChatRoom/Create
        [HttpPost]
        public ActionResult Create(Loggin info)
        {

            ChatRoom roomChat;
            Usuario user;
            ChatRoomUsuario userRoom;

            bool saveChange = false;
            bool isOwner = false;

            if (ModelState.IsValid)
            {
                user = unitOfWork.UsuarioRepository.FindBy(m => m.NickName == info.NickName).FirstOrDefault();
                if (user == null)
                {
                    user = new Usuario();

                    user.NickName = info.NickName;
                    user.FechaCreacion = DateTime.Now;
                    user.Activo = true;
                    user.Correo = info.NickName + "@chat1.org";
                    unitOfWork.UsuarioRepository.Add(user);
                    saveChange = true;
                }

                roomChat = unitOfWork.ChatRoomRepository.FindBy(m => m.Nombre == info.chatName).FirstOrDefault();
                if (roomChat == null)
                {
                    roomChat = new ChatRoom();

                    roomChat.Nombre = info.chatName;
                    roomChat.Activo = true;
                    roomChat.Descripcion = info.chatName + "  by  " + info.NickName;
                    isOwner = true;
                    unitOfWork.ChatRoomRepository.Add(roomChat);
                    saveChange = true;

                }

                if (saveChange)
                    unitOfWork.Save();

                roomChat = unitOfWork.ChatRoomRepository.FindBy(m => m.Nombre == info.chatName).FirstOrDefault();
                userRoom = unitOfWork.ChatRoomUsuarios.FindBy(m => m.UsuarioID == user.UsuarioID && m.ChatRoomID == roomChat.ChatRoomID).FirstOrDefault();
                if (userRoom == null) //Si no habia  participado en el chat
                {
                    userRoom = new ChatRoomUsuario();
                    userRoom.UsuarioID = user.UsuarioID;
                    userRoom.ChatRoomID = roomChat.ChatRoomID;
                    userRoom.EnLinea = true;
                    userRoom.FechaCreacion = DateTime.Now;
                    userRoom.Creador = isOwner;
                    unitOfWork.ChatRoomUsuarios.Add(userRoom);
                    unitOfWork.Save();

                }
                else
                {
                    if (!userRoom.EnLinea.Value)
                    {
                        userRoom.EnLinea = true;
                        unitOfWork.ChatRoomUsuarios.Update(userRoom);
                        unitOfWork.Save();
                    }
                }
                //No es necesario TODO eliminar
                TempData["USERID"] = info.NickName;
                TempData["ROOMPK"] = roomChat.ChatRoomID;
                TempData["USERIDPK"] = userRoom.UsuarioID;
                TempData["ROOMNAME"] = info.chatName;


                //Guardar usuario en la session , es para que puede ser accedido en cualquier controller
                ChatRoomUsuarioInfo userInfo = new ChatRoomUsuarioInfo(userRoom.ChatRoomUsuarioID,user);
                Session["Usuario"] = userInfo;
                //Guarda el Id del Chat en la session
                Session["ChatId"] = roomChat.ChatRoomID;


                return RedirectToAction("Room", "ChatRoom", new { id = roomChat.ChatRoomID });
            }

            return Redirect("Index");

        }

        // GET: ChatRoom/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ChatRoom/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ChatRoom/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ChatRoom/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [ValidaSesionActionFilter]
        public ActionResult CierraSesion(bool redirreciona=true)
        {
            ChatRoomUsuarioInfo usuario = (ChatRoomUsuarioInfo)Session["Usuario"];
            int ChatId = int.Parse(Session["ChatId"].ToString());
            try
            {
                unitOfWork.ChatRoomUsuarios.FueraDeLinea(usuario.User.UsuarioID, ChatId);
                unitOfWork.Save();
            }
            catch (Exception e)
            {
                throw e;
            }
            Session["Usuario"] = null;
            Session["ChatId"] = null;
            if (redirreciona)
                return RedirectToAction("Index", "Home");
            else
                return new EmptyResult();


        }


    }
}
