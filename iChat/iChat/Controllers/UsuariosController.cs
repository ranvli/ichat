﻿using iChat.Controllers.ActionFilters;
using iChatEntities;
using iChatRepository;
using iChatRepository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iChat.Controllers
{
    public class UsuariosController : Controller

    {
        UnitOfWork unitOfWork = new UnitOfWork();

        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }
        [ValidaSesionActionFilter]
        public ActionResult GetUsuarios()
        {
            int ChatId = int.Parse(Session["ChatId"].ToString());
            IEnumerable<Usuario> usuarios = unitOfWork.UsuarioRepository.GetUsuariosEnLinea(ChatId);
            PartialViewResult aux  = PartialView("Usuarios", usuarios);
            return aux;


        }
      

        
    }
}