﻿using iChatRepository.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using iChatEntities;
using System.Collections.Concurrent;

namespace iChat.Socket
{
    class IChatConexionCliente
    {
        static readonly MensajeList Mensajes = new MensajeList();

        UnitOfWork unitOfWork = new UnitOfWork();
        //Usuario que escribe mensajes
        ChatRoomUsuarioInfo usuarioInfo = null;
        //Chat donde se escriben los mensajes
        int ChatId = -1;

        //WebSocket del cliente
        public WebSocket socket { get; set; }

        public event EventHandler<CierraSocketEventArgs> CierraConexionSocket;
        protected virtual void OnCierraConexionSocket(CierraSocketEventArgs e)
        {
            EventHandler<CierraSocketEventArgs> handler = CierraConexionSocket;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public IChatConexionCliente(WebSocket socket, ChatRoomUsuarioInfo usuario, int ChatId)
        {
            this.socket = socket;
            this.usuarioInfo = usuario;
            this.ChatId = ChatId;
        }

        void EnviarMensajes(IEnumerable<ChatMensajeDTO> MensajesNuevos)
        {
            var message = new { MensajesNuevos };
            EnviarJsonMessage(message);
        }


        void EnviarJsonMessage(object message)
        {
            try
            {
                lock (socket)
                {
                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(message);
                    var buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));

                    socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None).Wait();
                }
            } catch (Exception e)
            {
                throw e;
            }
        }

        public async Task Start()
        {
            //Si hay un cambio en los mensajes,lo manda en el socket del cliente
            Mensajes.CollectionChanged += (Enviarer, args) =>
            {
                switch (args.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        EnviarMensajes(args.NewItems.Cast<ChatMensajeDTO>());
                        break;
                }
            };
            //Mientras el socket del cliente este abierto se esperan mensajes.
            while (socket.State == WebSocketState.Open)
            {
                var message = await RecibeMensaje();
                if (message == null) continue;
                if (message.ContainsKey("msj"))
                {
                    NuevoMensaje(message);
                }

            }
            //Si el cliente , cierra session o la venta , cierra su socket

            if (socket.State == WebSocketState.Closed ||
                socket.State == WebSocketState.CloseReceived || 
                    socket.State == WebSocketState.CloseReceived ||
                    socket.State == WebSocketState.Aborted)
            {
                CierraSocketEventArgs args = new CierraSocketEventArgs();
                args.ChatRoomUsuarioId = this.usuarioInfo.ChatRoomUsuarioId;
                OnCierraConexionSocket(args);
                //Mensajes.Clear();
                //var t = socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed in server by the client", CancellationToken.None);
                //t.Wait();
                //if (t.IsCompleted)
                //    t = null;
            }
        }

     

        void NuevoMensaje(IDictionary<string, object> message)
        {
            try
            {
                
                var msj = (string)message["msj"];
                iChatEntities.ChatMensaje mensaje = new iChatEntities.ChatMensaje();
                mensaje.Texto = msj;
                mensaje.FechaCreacion = DateTime.Now;
                mensaje.UsuarioCreacionID = usuarioInfo.User.UsuarioID;
                mensaje.Usuario_UsuarioID = mensaje.UsuarioCreacionID;
                mensaje.ChatRoomID = ChatId;
                unitOfWork.ChatMensajesRepository.Add(mensaje);
                unitOfWork.Save();
                iChatEntities.ChatDTO chatDTO = new iChatEntities.ChatDTO();
                iChatEntities.ChatMensajeDTO mensajeDTO = new iChatEntities.ChatMensajeDTO();
                mensajeDTO.Texto = mensaje.Texto;
                mensajeDTO.FechaCreacion = mensaje.FechaCreacion.ToString();
                mensajeDTO.NickName = usuarioInfo.User.NickName;
                //Podria viajar encriptado
                mensajeDTO.ChatMensajeID = mensaje.ChatRoomID;
                chatDTO.ChatMensajes.Add(mensajeDTO);
                Mensajes.Add(mensajeDTO);
                //EnviarJsonMessage(chatDTO);
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        
        //Espera mensajes del client
        async Task<IDictionary<string, object>> RecibeMensaje()
        {
            var buffer = new ArraySegment<byte>(new byte[1024]);
            var result = await socket.ReceiveAsync(buffer, CancellationToken.None);
            //Los bytes se convierte en texto
            var json = Encoding.UTF8.GetString(buffer.Array, buffer.Offset, result.Count);
            var serializer = new JavaScriptSerializer();
            return (IDictionary<string, object>)serializer.DeserializeObject(json);
        }

        class Mensaje
        {
            static int _sgtId = 1000;

            public Mensaje(string text)
            {
                id = Interlocked.Increment(ref _sgtId);
                this.texto = text;
            }

            public Mensaje(int id, string texto)
            {
                this.id = id;
                this.texto = texto;
            }

            public string texto { get; set; }
            public int id { get; set; }
        }

        class MensajeList : ObservableCollection<ChatMensajeDTO>
        {
        }
       
    }
}