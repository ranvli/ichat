﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Web.WebSockets;
using iChatEntities;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using Newtonsoft.Json;

namespace iChat.Socket
{
    public class IChatHttpHandler:IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        ChatRoomUsuarioInfo usuario = null;
        int ChatId = -1;
        static ConcurrentDictionary<int, WebSocket> clientes = new ConcurrentDictionary<int, WebSocket>();
        public void ProcessRequest(HttpContext context)
        {
            // Si el usuario no esta logueado o la sesión expiro , lo devuelve a loguearse a la pantalla de login.
            if (context.Session["Usuario"] == null || context.Session["ChatId"] == null)
            {
                context.Response.Redirect("~/Home");
               

            }
            else
            {
                //Si es una conexion de WebSocket , se acepta, por conexión la aplicacion creara, un Task que atenderá la conexión
                if (context.IsWebSocketRequest)
                {
                    //string accion = (context.Request.QueryString["accion"] != null) ? context.Request.QueryString["accion"] : "";

                    usuario = (ChatRoomUsuarioInfo)context.Session["Usuario"];
                    int.TryParse(context.Session["ChatId"].ToString(), out ChatId);


                    context.AcceptWebSocketRequest(ProcessWebSocketRequest);

                }
               
            }
        }

        Task ProcessWebSocketRequest(AspNetWebSocketContext context)
        {
            //Recibe socket del cliente
            var socket = context.WebSocket;
            //Abre conecion para el socket del cliente
           IChatConexionCliente connection = new IChatConexionCliente(socket, this.usuario, this.ChatId);
            connection.CierraConexionSocket += (s, e) =>
            {
                // when the connection ends, try to remove the user
                WebSocket aux;
                if (clientes.TryRemove(e.ChatRoomUsuarioId, out aux))
                {
                    if (aux != connection.socket)
                    {
                        // whops! user reconnected too fast and you are removing
                        // the new connection, put it back
                        clientes.AddOrUpdate(this.usuario.ChatRoomUsuarioId, aux, (p, w) => aux);
                    }
                }
            };
            
            clientes.AddOrUpdate(this.usuario.ChatRoomUsuarioId, socket, (p, w) => connection.socket);
            //Socket del cliente inicia
            Task t = connection.Start();
           


            return t;
        }

       

        public bool IsReusable
        {
            get { return false; }
        }
    }
}