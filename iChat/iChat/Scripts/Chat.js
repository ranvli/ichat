﻿(function () {
    var host = null;
    var socketURL = "";
    var socket = null;
   
    var intervalUsuarios = null;

    var Chat = Object.inherit({

     
        initialize: function (socket, sectionElement) {
            this.initializeSocket(socket);
            this.initializeUI(sectionElement);
        },

        initializeSocket: function (socket) {
            this.socket = socket;
            // Se asigna funciona que procesa los mensajes que envia el servidor
            this.socket.onmessage = this.recibeMensaje.bind(this);
            this.socket.onclose = this.cierraSocket.bind(this);
        },

        initializeUI: function (sectionElement) {
            this.mensajesListElement = document.getElementById("dMensajes");
            this.mensajesListElement.scrollTop = this.mensajesListElement.scrollHeight;
            document.getElementById("btnEnviar").addEventListener("click", this.mensaje.bind(this), false);
            $('#btnCerrarSesion').click(function () { CierraSesion(true); });

        },
        reConecta: function () {
            this.socket = new WebSocket(socketURL);
            this.socket.onmessage = this.recibeMensaje.bind(this);
            this.socket.onclose = this.cierraSocket.bind(this);
        },
        mensaje: function (event) {
            //Esta función captura el mensaje cuando el usuario presiona el boton enviar
            event.preventDefault();

            //var text = $('#mensajeTexto').val();
            var text = $('textarea#textAreaMsn').val();
            // Limpia para un nuevo mensaje
          //  $('#mensajeTexto').val('');
            $('textarea#textAreaMsn').val('');
            $("textarea#textAreaMsn").next().empty();
            $("textarea#textAreaMsn").next().text("");
            if (text) {
                this.enviarMensaje(text);
            }
        },
        enviarMensaje: function (text) {
            // Esta función dibuja el mensaje en pantalla
            //La información del usuario la toma del session
            var message = {
                msj: text              
            };
            
            var json = JSON.stringify(message);
            if (this.socket == null)
                this.reConecta();
            var readyState = this.socket.readyState;
            //0 Conectando
            //1 Conexión Abierta
            //2 Conexión Cerrandose
            //3 Conexión Cerrada
            if (readyState == 1)
                this.socket.send(json);
            else {
                alert('Conexión Cerrada');

            }

            
        },
        cierraSocket: function (event) {
                        //Se ejecuta cuando el socket se cierra
            this.socket.close();
            this.socket = null;
            if (socket != null) {
                socket.close();
                socket = null;
            }
    
        },
        recibeMensaje: function (event) {
            // Esta función sucede cuando el servidor devuelve datos del socket
            //Recibe array de mensajes nuevos
            var mensajes = JSON.parse(event.data);

            // Si tiene comentarios nuevos
            if (mensajes.MensajesNuevos) {
                this.desplegarMensajes(mensajes);
            }

        },

        desplegarMensajes: function (mensajes) {
            //Esta funcion despliega los comentarios en pantalla

            mensajes.MensajesNuevos.forEach(this.mostrarMensajes, this);
        },


        mostrarMensajes: function (mensaje) {
            var item = this.crearMensajesItem(mensaje);
            //item.appendChild(this.createReportLink());
            this.mensajesListElement.appendChild(item);
           // var divPaneliChat = document.getElementById("dMensajes");
            this.mensajesListElement.scrollTop = this.mensajesListElement.scrollHeight;
        },

        crearMensajesItem: function (mensaje) {
           

            var node = document.createElement("div");
            node.className = "bubble";

        

            if ($('#USERID').val() == mensaje.NickName) {
                node.className = " bubble darker";
                node.innerHTML = '<img src ="/Content/images/robot.png" alt = "Avatar" class="right" /> '  +
                       "<p><b>" + mensaje.NickName +"</b></p>" +
                    "<p>"+ mensaje.Texto +"</p>" +
                " <span class='time-left'> "+  mensaje.FechaCreacion+" </span>";
                    
            } else {

                node.innerHTML = "<img src = '/Content/images/robot.png' alt = 'Avatar'/> " +
                    "<p><b>" + mensaje.NickName + "</b></p>" +
                    "<p>" + mensaje.Texto + "</p>" +
                    " <span class='time-right'> " + mensaje.FechaCreacion + " </span>";
            }

         
            return node;
        },

        noClic: function (event) {
            event.preventDefault();
        }


    });

    function ActualizaUsuarios() {
        $.ajax({
            type: "GET",
           
            contentType: "application/json; charset=utf-8",
            cache: false,
            url: URL_Acciones.GetUsuarios,
            timeout: 9000,
            
            success: (function (data, textStatus, xhr) {

                $('#dUsuariosChat').empty();
                $('#dUsuariosChat').append(data);

            }),
            error: (function (data) {

                alert(data.responseText);
            })
        });
    }
    function CierraSesion(redirreciona) {
        
       
        if (redirreciona) {
            
            window.location.href = window.location.origin + URL_Acciones.CierraSesion;
           
        } else {
            $.ajax({
                type: "GET",

                contentType: "application/json; charset=utf-8",
                cache: false,
                url: URL_Acciones.CierraSesion,
                timeout: 9000,
                data: {
                redirreciona:redirreciona

                },
                error: (function (data) {

                    alert(data.responseText);
                })
            });
        }
    }

    function connectSocket(){
        host = window.location.href.split("/")[2];
        socketURL = "ws://" + host + "/Socket/socket.ashx";
        socket = new WebSocket(socketURL);
        Chat.create(
            socket,
            document.querySelector("section.live")
        );
    }
    function InitChat() {
        connectSocket();
        intervalUsuarios = setInterval(ActualizaUsuarios, 2000)
        $(window).bind('beforeunload', function (e) {

            CierraSesion(false);
            return null;

        });
        $(document).keypress(function (e) {
            if (e.which == 13) {

                $("#btnEnviar").click();

            }
        });
    }

    InitChat();
   
   

}());