﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using iChat.Models;
using iChatEntities;

namespace iChat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Seed the database with sample data for development. This code should be removed for production.
            //Database.SetInitializer<iChatModelContainer>(new IChatInitializer());
            //iChatModelContainer db = new iChatModelContainer();
            //db.Database.Initialize(true);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
      
       
    }
}
