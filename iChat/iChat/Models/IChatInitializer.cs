﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using iChatEntities;


namespace iChat.Models
{
    public class IChatInitializer 
        :DropCreateDatabaseAlways<iChatModelContainer>
    {
        protected override void Seed(iChatModelContainer db)
        {
            base.Seed(db);
            var user = new Usuario
            {
                Activo = true,
                Correo = "nandobv@gmail.com",
                NickName = "nandobv",
                FechaCreacion = DateTime.Today
            };
            db.Usuarios.Add(user);
            db.SaveChanges();
            var chats = new List<ChatRoom>
            {
                new ChatRoom
                {
                    Nombre ="Test1",
                    Descripcion="Texto de prueba",
                
                    Activo=true

                },
                new ChatRoom
                 {
                    Nombre ="Test2",
                    Descripcion="Texto2 de prueba",
                 
                     Activo=true

                }
            };
           // chats.ForEach(c => db.ChatRooms.Add(c));
           foreach (ChatRoom c in chats)
            {
                db.ChatRooms.Add(c);
               
                ChatRoomUsuario chatUsuario = new ChatRoomUsuario();
                chatUsuario.ChatRoomID = c.ChatRoomID;
                chatUsuario.FechaCreacion = DateTime.Now;
                chatUsuario.UsuarioID = 1;
                chatUsuario.EnLinea = true;
                chatUsuario.Creador = true;
              //  db.ChatRoomUsuarios.Add(chatUsuario);
           
            }
            db.SaveChanges();

        }
    }
}