﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatEntities
{
    public class ChatMensajeDTO
    {
        public ChatMensajeDTO() { }
        public int ChatMensajeID { get; set; }
        public string Texto { get; set; }
        public string NickName { get; set; }
        public string FechaCreacion { get; set; }
    }
}
