﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatEntities
{
    public class ChatRoomUsuarioInfo
    {
        //Id del Chat x Usuario
         public int ChatRoomUsuarioId { get; set; }
         public Usuario User { get; set; }

        public ChatRoomUsuarioInfo(int ChatRoomUsuarioId,Usuario user)
        {
            this.User = user;
            this.ChatRoomUsuarioId = ChatRoomUsuarioId;
        }


    }
}
