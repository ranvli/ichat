﻿using iChatEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace iChatEntities
{
    public class ChatRoomInfo
    {
        public ChatRoomInfo() {

           

        }
      
        public int ChatRoomID { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public string Descripcion { get; set; }
        [Display(Name ="Fecha Creación")]
        public DateTime FechaCreacion { get; set; }
        [Display(Name = "Usuario Creación")]
        public string CorreoUsuarioCreacion { get; set; }

     

    }
}