﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatEntities
{
   public  class ChatDTO
    {
        public  ChatDTO() {
            this.ChatMensajes = new List<ChatMensajeDTO>();
            this.Info = new ChatRoomInfo();
        }
        public ChatRoomInfo Info { get; set; }
        public List<ChatMensajeDTO> ChatMensajes { get; set; }


    }
}
