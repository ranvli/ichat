﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iChatShare
{
    public static class DataBaseHelpers
    {

        private static string _connectionString = "";
        private static string GetWorkingCS()
        {
            GC.KeepAlive(_connectionString);

            if (_connectionString != null & _connectionString != "")
            {
                return _connectionString;
            }

            //string DefaultSQLServer = @"(local)\SQLEXPRESS";
            bool connected = false;

            while (!connected)
            {
                if (_connectionString == "")
                {
#if (DEBUG)
                    _connectionString += ConfigurationManager.ConnectionStrings["DEV"].ConnectionString;
#elif (Test)
                    _connectionString += ConfigurationManager.ConnectionStrings["DEV"].ConnectionString;
#else
                    _connectionString +=  ConfigurationManager.ConnectionStrings["PROD"].ConnectionString;
#endif
                }
                try
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        connected = true;
                    }
                }
                catch (Exception ex)
                {
                    //aquí en caso de una app desktop se preguntaría por la conexión de la bd local, pero
                    //estamos en una app web, por lo que se debe especificar de acuerdo a la pc local
                    //sin embargo podemos hacerlo en la variable de arriba llamada DefaultSQLServer
                    //pero les queda esta parte por si quiere usar este snippet de código para su app desktop
                    _connectionString = "";
                    //en este caso terminamos app lanzando la excepción
                    throw ex;
                }
            }

            _connectionString = _connectionString.Replace("master", "chat1org");


            return _connectionString;

        }

       
        public static string GetBaseConnectionFromAppConfig(bool addProvider)
        {


            string provider = "Provider=SQLOLEDB.1;";
            string csBuilt = "";

            if (addProvider) csBuilt = provider;

            csBuilt += GetWorkingCS();

            //#if (DEBUG)
            //            csBuilt += ConfigurationManager.ConnectionStrings["DEV"].ConnectionString;
            //#elif (Test)
            //            csBuilt += ConfigurationManager.ConnectionStrings["DEV"].ConnectionString;
            //#else
            //                csBuilt +=  ConfigurationManager.ConnectionStrings["PROD"].ConnectionString;
            //#endif

            return csBuilt;

        }

        public static string GetEntityConnection(string connectionString = "")
        {
            var entityConnectionStringBuilder = new EntityConnectionStringBuilder();
            entityConnectionStringBuilder.Provider = "System.Data.SqlClient";
            entityConnectionStringBuilder.ProviderConnectionString = connectionString == "" ? GetBaseConnectionFromAppConfig(false) : connectionString;
            entityConnectionStringBuilder.Metadata = "res://*/iChatModel.csdl|res://*/iChatModel.ssdl|res://*/iChatModel.msl";
            return entityConnectionStringBuilder.ToString();
        }

        public static string GetCurrentEnvironment()
        {
#if (DEBUG)
            return "debug";
#elif (Test)
            return "test";
#else
                            return "production";
#endif
        }

        /// <summary>
        /// Method to check if a database exists
        /// </summary>
        /// <param name="conn">Connection string</param>
        /// <param name="databaseName">Database name to check</param>
        /// <returns>Returns boolean indicating if a database exist or not</returns>
        public static bool CheckDatabaseExists(string conn, string databaseName)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);

                using (SqlConnection tmpConn = new SqlConnection(conn))
                {
                    using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                    {
                        tmpConn.Open();

                        object resultObj = sqlCmd.ExecuteScalar();

                        int databaseID = 0;

                        if (resultObj != null)
                        {
                            int.TryParse(resultObj.ToString(), out databaseID);
                        }

                        tmpConn.Close();

                        result = (databaseID > 0);
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

    }
}
